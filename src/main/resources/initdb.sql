create table users (
                       id bigserial not null,
                       username varchar(255) not null,
                       password varchar(255) not null,
                       role varchar(255) not null,
                       primary key(id)
);

alter table users
    add constraint users_username_unique unique (username);


create table expositions (
                             id bigserial not null,
                             title varchar(255) not null,
                             cost int8 not null,
                             date date not null,
                             status varchar(255) not null,
                             primary key(id)
);

create table halls (
                       id bigserial not null,
                       number int8 not null,
                       seats int8 not null,
                       primary key(id)
);

create table sessions (
                          id bigserial not null,
                          exposition_id int8 not null,
                          hall_id int8 not null,
                          primary key(id)
);

alter table sessions
    add constraint expositions_fk foreign key (exposition_id) references expositions,
add constraint halls_fk foreign key (hall_id) references halls,
add constraint exposition_hall_unique unique (exposition_id, hall_id);

create table tickets (
                         id bigserial not null,
                         exposition_id int8 not null,
                         user_id int8 not null,
                         primary key(id)
);

alter table tickets
    add constraint expositions_fk foreign key (exposition_id) references expositions,
add constraint users_fk foreign key (user_id) references users;

insert into halls (number, seats) values (1, 100);
insert into halls (number, seats) values (2, 300);
insert into halls (number, seats) values (3, 50);
insert into halls (number, seats) values (4, 400);