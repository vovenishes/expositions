package com.example.expositions1.controllers.command.ticket;

import com.example.expositions1.controllers.command.Command;
import com.example.expositions1.exceptions.InternalDatabaseException;
import com.example.expositions1.exceptions.ModelNotFoundException;
import com.example.expositions1.exceptions.ValidationException;
import com.example.expositions1.services.ExpositionService;
import com.example.expositions1.services.TicketService;
import com.example.expositions1.services.impl.ExpositionServiceImpl;
import com.example.expositions1.services.impl.TicketServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TicketBuyCommand implements Command {

    private static final Pattern ID_REGEX = Pattern.compile("(?<=\\/buy\\/)[0-9]+(?=\\/?$)");

    private TicketService ticketService;

    public TicketBuyCommand() {
        this.ticketService = new TicketServiceImpl();
    }

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {

        Long id = null;
        try {
            id = Long.valueOf(getExpositionId(request));
        } catch (ValidationException e) {
            e.printStackTrace();
        }

        try {
            ticketService.buyExpositionByIdForUser(id, request.getSession().getAttribute("username"));
        } catch (InternalDatabaseException e) {
            e.printStackTrace();
        }


        return "redirect:/overview";
    }


    private static String getExpositionId(HttpServletRequest request) throws ValidationException {
        Matcher matcher = ID_REGEX.matcher(request.getRequestURI());
        if (matcher.find()) {
            return matcher.group();
        }
        throw new ValidationException("Not valid");
    }

}
