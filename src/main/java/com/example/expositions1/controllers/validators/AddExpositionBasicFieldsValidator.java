package com.example.expositions1.controllers.validators;

import com.example.expositions1.dto.exposition.AddExpositionData;
import com.example.expositions1.exceptions.ValidationException;

import static java.util.Objects.isNull;

public class AddExpositionBasicFieldsValidator implements Validator<AddExpositionData>{
    @Override
    public void validate(AddExpositionData request) throws ValidationException {
        if (isNull(request.getTitle())
                || isNull(request.getCost())
                || isNull(request.getDate())
        || request.getTitle().isBlank()
                || request.getCost().isBlank()
                || request.getDate().isBlank()) {
            throw new ValidationException("Fields is null");
        }
    }
}
