package com.example.expositions1.controllers.converters;

import com.example.expositions1.dto.page.Direction;
import com.example.expositions1.dto.page.PageFilters;
import com.example.expositions1.dto.page.SearchParams;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

import static com.example.expositions1.constants.Parameters.Defaults.*;
import static com.example.expositions1.constants.Parameters.Keys.*;
import static java.util.Objects.nonNull;

public class ProductConverter implements Converter<HttpServletRequest, SearchParams> {

    private HttpServletRequest request;

    @Override
    public SearchParams convert(HttpServletRequest request) {
        this.request = request;
        return new SearchParams(
                getSortField(SORT_FIELD, SORT_FIELD_DEFAULT),
                getDirection(DIRECTION, DIRECTION_DEFAULT),
                getSearchQuery(SEARCH_QUERY, SEARCH_QUERY_DEFAULT),
                getDigitParam(PAGE, PAGE_DEFAULT),
                getDigitParam(PAGE_SIZE, PAGE_SIZE_DEFAULT)
        );
    }

    private String getSortField(String key, String defaultVal) {
        String field = this.request.getParameter(key);
        return field != null && sortFieldIsCorrect(field) ? field : defaultVal;
    }

    private boolean sortFieldIsCorrect(String field) {
        boolean isCorrect =  Arrays.stream(PageFilters.values())
                .map(PageFilters::getSortField)
                .anyMatch(field::equals);
        return isCorrect;
    }

    private String getDirection(String key, String defaultVal) {
        String field = this.request.getParameter(key);
        return field != null && directionIsCorrect(field) ? field : defaultVal;
    }

    private boolean directionIsCorrect(String field) {
        boolean isCorrect =  Arrays.stream(Direction.values())
                .map(Direction::getDirection)
                .anyMatch(field::equals);
        return isCorrect;
    }

    private String getSearchQuery(String key, String defaultVal) {
        String field = request.getParameter(key);
        return field != null ? field : defaultVal;
    }


    private Integer getDigitParam(String key, String defaultVal) {
        try {
            Integer param = Integer.parseInt(this.request.getParameter(key));
            if (nonNull(param)) {
                return param;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            // TODO: log
        }
        return Integer.parseInt(defaultVal);
    }
}
