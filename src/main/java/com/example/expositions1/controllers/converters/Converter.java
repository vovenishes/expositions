package com.example.expositions1.controllers.converters;

import com.example.expositions1.exceptions.ValidationException;

import javax.servlet.http.HttpServletRequest;

public interface Converter<REQ, RES> {

     RES convert(REQ request);

}
