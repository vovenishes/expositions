package com.example.expositions1.dto.page;

public enum PageFilters {

    ID("id"),
    TITLE("title"),
    COST("cost"),
    HALLS("halls"),
    DATE("date");

    private String sortField;

    PageFilters(String sortField) {
        this.sortField = sortField;
    }

    public String getSortField() {
        return this.sortField;
    }


}
