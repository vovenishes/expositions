package com.example.expositions1.dto.page;

public class SearchParams {

    private String sortField;
    private String direction;
    private String searchQuery;
    private Integer page;
    private Integer size;

    public SearchParams(String sortField, String direction, String searchQuery, Integer page, Integer size) {
        this.sortField = sortField;
        this.direction = direction;
        this.searchQuery = searchQuery;
        this.page = page;
        this.size = size;
    }


    public String getFilterField() {
        return sortField;
    }

    public void setFilterField(String filterField) {
        this.sortField = filterField;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
}
