package com.example.expositions1.dto.page;

import java.util.List;

public class Page<T> {
    List<T> items;
    Boolean hasContent;
    Integer total;
    Integer number;

    public Page() {}

    public Page(List<T> items, Boolean hasContent, Integer total, Integer number) {
        this.items = items;
        this.hasContent = hasContent;
        this.total = total;
        this.number = number;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    public Boolean getHasContent() {
        return hasContent;
    }

    public void setHasContent(Boolean hasContent) {
        this.hasContent = hasContent;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

}

