package com.example.expositions1.models;

public class HallModel {

    private Long id;
    private Integer number;
    private Integer seats;

    public HallModel() {
    }

    public HallModel(Long id, Integer number, Integer seats) {
        this.id = id;
        this.number = number;
        this.seats = seats;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getSeats() {
        return seats;
    }

    public void setSeats(Integer seats) {
        this.seats = seats;
    }
}
