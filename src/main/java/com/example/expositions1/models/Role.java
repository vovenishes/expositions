package com.example.expositions1.models;


import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.example.expositions1.security.AuthoritiEndpointsPatterns.*;

public enum Role {

//TODO пермитед олл сейчас, а нужно для разных пользователей разные доступные ссылки


    //Користувач може переглядати експозиції за темою,
    //    вартістю квитка,
    //    а також фільтрувати за датою проведення.
    //ExpList.jsp
    GUEST(Stream.concat(PERMITTED_ALL.stream(), PERMITTED_GUEST.stream()).collect(Collectors.toList())),

    // Авторизований користувач може купити квиток на обрану експозицію.

    USER(PERMITTED_ALL),

    //Адміністратор робить список експозицій (тема, зал, період та час роботи, вартість квитка),
    //    а також може відміняти експозиції,
    //    переглядати статистику відвідувань.
    //    Експозиція може займати один або декілька залів.

//    ADMIN(PERMITTED_ALL);PERMITTED_ADMIN

    ADMIN(Stream.of(
                    PERMITTED_ALL.stream(),
                    PERMITTED_ADMIN.stream(),
                    PERMITTED_AUTHORIZED.stream()
            )
            .reduce(Stream::concat)
            .orElseGet(Stream::empty)
            .collect(Collectors.toList()));

    private List<String> endpoints;

    private Role(List<String> matchers) {

        this.endpoints = matchers;
    }

    public List<String> getEndpointPatterns() {
        return this.endpoints;
    }

}
