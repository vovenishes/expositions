package com.example.expositions1.utils;

import com.example.expositions1.exceptions.InternalDatabaseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class LangProperties {

    private static final Logger LOGGER = LoggerFactory.getLogger(LangProperties.class.getName());

    private Properties prop;


    private LangProperties() {
    }

    public static Properties getProperties(String lang) {
        try {
            return getPropertiesByLang(lang);
        } catch (InternalDatabaseException e) {
            e.printStackTrace();
            return new Properties();
        }
    }


    public static String getProperty(String key, String lang) {
        try {
            return getPropertiesByLang(lang).getProperty(key);
        } catch (InternalDatabaseException e) {
            e.printStackTrace();
            return "translate not found";
        }
    }


    private static synchronized Properties getPropertiesByLang(String lang) throws InternalDatabaseException {
        String filePath = String.format("messages_%s.properties", "ru".equalsIgnoreCase(lang) ? "ru" : "en");
        Properties properties = new Properties();

        try (InputStream inputStream = LangProperties.class.getClassLoader().getResourceAsStream(filePath);
             Reader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8)) {
            properties.load(reader);
            return properties;

        } catch (IOException ex) {
            ex.printStackTrace();
            throw new InternalDatabaseException();
        }
    }

}