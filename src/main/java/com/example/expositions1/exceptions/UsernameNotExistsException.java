package com.example.expositions1.exceptions;

public class UsernameNotExistsException extends Exception {
    public UsernameNotExistsException() {
    }

    public UsernameNotExistsException(String message) {
        super(message);
    }
}
