package com.example.expositions1.constants;

public interface Endpoints {

    interface Regex {
        String EXPOSITION_LIST_R = "\\/overview\\/?";
        String REGISTRATION_R = "\\/registration\\/?$";
        String LOGIN_R = "\\/login\\/?$";
        String ADD_EXPOSITION_R = "\\/add\\/?$";
        String BUY_EXPOSITION_R = "\\/buy\\/[0-9]+\\/?$";
        String CANCEL_EXPOSITION_BY_ID_R = "\\/cancel\\/[0-9]+\\/?$";
        String LOGOUT_R = "\\/logout\\/?$";
    }



}
