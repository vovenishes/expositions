package com.example.expositions1.database.dao;

import com.example.expositions1.dto.exposition.ExpositionData;
import com.example.expositions1.dto.page.Page;
import com.example.expositions1.dto.page.SearchParams;
import com.example.expositions1.exceptions.InternalDatabaseException;
import com.example.expositions1.models.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class JDBCExpositionDao extends AbstractDao {

    private static final Logger LOG = LoggerFactory.getLogger(JDBCUserDao.class);

    public JDBCExpositionDao(Connection connection) {
        super(connection);
    }

    public Page<ExpositionData> findPageableExpositionsBySearchParams(SearchParams params) {
        String query = String.format("SELECT " +
                "EXPOSITIONS.ID, EXPOSITIONS.DATE, EXPOSITIONS.COST, TITLE, STRING_AGG(HALLS.NUMBER::VARCHAR(255), ', ') halls, " +
                "(select count(*) from (SELECT count(EXPOSITIONS.ID) over() " +
                "FROM ((EXPOSITIONS INNER JOIN SESSIONS ON EXPOSITIONS.ID = SESSIONS.EXPOSITION_ID ) INNER JOIN HALLS ON HALLS.ID = SESSIONS.HALL_ID) WHERE expositions.status = 'ACTIVE' AND (TITLE LIKE ? OR CAST(COST AS TEXT) LIKE ?) GROUP BY EXPOSITIONS.ID) as pages) as total " +
                "FROM ((EXPOSITIONS INNER JOIN SESSIONS ON EXPOSITIONS.ID = SESSIONS.EXPOSITION_ID ) INNER JOIN HALLS ON HALLS.ID = SESSIONS.HALL_ID) WHERE expositions.status = 'ACTIVE' AND (TITLE LIKE ? OR CAST(COST AS TEXT) LIKE ?) GROUP BY EXPOSITIONS.ID " +
                "ORDER BY %s %s " +
                "LIMIT ? " +
                "OFFSET ?", params.getFilterField(), params.getDirection());
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, "%" + params.getSearchQuery() + "%");
            statement.setString(2, "%" + params.getSearchQuery() + "%");
            statement.setString(3, "%" + params.getSearchQuery() + "%");
            statement.setString(4, "%" + params.getSearchQuery() + "%");
            statement.setInt(5, params.getSize());
            statement.setInt(6, params.getSize() * (params.getPage() - 1));
            try (ResultSet resultSet = statement.executeQuery()) {
                List<ExpositionData> expositions = new ArrayList<>();
                Long expositionsCount = 0L;
                while (resultSet.next()) {
                    expositions.add(getExpositionFromResultSet(resultSet));
                    if (expositionsCount == 0) {
                        expositionsCount = resultSet.getLong("total");
                    }
                }
                Integer totalPages = (int) (expositionsCount / params.getSize() + ((expositionsCount % params.getSize() == 0) ? 0 : 1));
                return new Page<>(expositions, !expositions.isEmpty(), totalPages, params.getPage());
            }
        } catch (SQLException e) {
            e.printStackTrace();
            //TODO: log and throw
            e.printStackTrace();
        }


        return null;
    }

    private static ExpositionData getExpositionFromResultSet(ResultSet res) throws SQLException {
        ExpositionData data = new ExpositionData();
        data.setId(res.getLong("id"));
        data.setTitle(res.getString("title"));
        data.setCost(String.valueOf(res.getLong("cost")));
        data.setDate(res.getTimestamp("date").toLocalDateTime().format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));
        data.setHalls(res.getString("halls"));
        return data;
    }


    public Optional<ExpositionModel> findLazyById(Long id) throws InternalDatabaseException {

        try (PreparedStatement statement = connection
                .prepareStatement("SELECT * FROM expositions WHERE id = ?")) {
            statement.setLong(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(getExpositionModelFromResultSet(resultSet));
                } else {
                    return Optional.empty();
                }
            }
        } catch (SQLException ex) {
            LOG.error("Exposition [{}] not found: {}", id, ex);
            throw new InternalDatabaseException(ex);
        }

    }

    private static ExpositionModel getExpositionModelFromResultSet(ResultSet res) throws SQLException {
        ExpositionModel exp = new ExpositionModel();
        exp.setId(res.getLong("id"));
        exp.setTitle(res.getString("title"));
        exp.setCost(res.getInt("cost"));
        exp.setDate(Date.valueOf(res.getTimestamp("date").toLocalDateTime().toLocalDate()));
        exp.setStatus(ExpositionStatus.valueOf(res.getString("status")));
        return exp;
    }

    public void update(ExpositionModel expositionModel) throws InternalDatabaseException {
        try (PreparedStatement statementUpdateExposition = connection
                .prepareStatement("update expositions SET title = ?,  cost = ?,  date = ?, status = ? where id = ?")) {

            statementUpdateExposition.setString(1, expositionModel.getTitle());
            statementUpdateExposition.setInt(2, expositionModel.getCost());
            statementUpdateExposition.setDate(3, expositionModel.getDate());
            statementUpdateExposition.setString(4, expositionModel.getStatus().toString());
            statementUpdateExposition.setLong(5, expositionModel.getId());


            statementUpdateExposition.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new InternalDatabaseException();
        }



    }
}
