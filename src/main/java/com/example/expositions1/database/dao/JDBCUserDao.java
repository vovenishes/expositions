package com.example.expositions1.database.dao;

import com.example.expositions1.exceptions.InternalDatabaseException;
import com.example.expositions1.models.Role;
import com.example.expositions1.models.UserModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class JDBCUserDao extends AbstractDao {
    private static final Logger LOG = LoggerFactory.getLogger(JDBCUserDao.class);

    public JDBCUserDao(Connection connection) {
        super(connection);
    }

    public void saveUser(UserModel userModel) throws InternalDatabaseException {
        try (PreparedStatement preparedStatement = connection
                .prepareStatement("INSERT INTO USERS (  USERNAME, PASSWORD, ROLE) VALUES (?,?,?) RETURNING ID")) {
            preparedStatement.setString(1, userModel.getUsername());
            preparedStatement.setString(2, userModel.getPassword());
            preparedStatement.setString(3, userModel.getRole().name());

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (!resultSet.next()) {
                    throw new SQLException();
                }
                LOG.info("User {} created with id {}", userModel.getUsername(), resultSet.getLong("id"));
            }
        } catch (SQLException ex) {
            LOG.error("User [{}] not saved: {}", userModel.getUsername(), ex);
            throw new InternalDatabaseException(ex);
        }
    }

    public Optional<UserModel> findByEmailAndPassword(String username, String password) throws InternalDatabaseException {

        try (PreparedStatement statement = connection
                .prepareStatement("SELECT * FROM USERS WHERE USERNAME = ? AND PASSWORD = ?")) {
            statement.setString(1, username);
            statement.setString(2, password);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    UserModel userModel = new UserModel();
                    userModel.setId(resultSet.getLong("id"));
                    userModel.setUsername(resultSet.getString("username"));
                    userModel.setPassword(resultSet.getString("password"));
                    userModel.setRole(Role.valueOf(resultSet.getString("role")));
                    return Optional.of(userModel);
                } else {
                    return Optional.empty();
                }
            }
        } catch (SQLException ex) {
            LOG.error("User [{}][{}] not found: {}", username, password, ex);
            throw new InternalDatabaseException(ex);
        }

    }

    public Optional<UserModel> findByEmail(String username) throws InternalDatabaseException {

        try (PreparedStatement statement = connection
                .prepareStatement("SELECT * FROM USERS WHERE USERNAME = ?")) {
            statement.setString(1, username);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    UserModel userModel = new UserModel();
                    userModel.setId(resultSet.getLong("id"));
                    userModel.setUsername(resultSet.getString("username"));
                    userModel.setPassword(resultSet.getString("password"));
                    userModel.setRole(Role.valueOf(resultSet.getString("role")));
                    return Optional.of(userModel);
                } else {
                    return Optional.empty();
                }
            }
        } catch (SQLException ex) {
            LOG.error("User [{}] not found: {}", username, ex);
            throw new InternalDatabaseException(ex);
        }
    }
}
