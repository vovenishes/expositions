package com.example.expositions1.database.connection;

import com.example.expositions1.exceptions.InternalDatabaseException;
import com.example.expositions1.utils.DBProperty;
import org.apache.commons.dbcp.BasicDataSource;

import javax.sql.DataSource;

public class ConnectionPool {

    private static DataSource dataSource;
    private static DBProperty statements;

    static {
        try {
            statements = DBProperty.getInstance();
        } catch (InternalDatabaseException e) {
            e.printStackTrace();
        }
    }

    private ConnectionPool() {
    }

    public static synchronized DataSource getDataSource() {

        if (dataSource == null) {
            BasicDataSource ds = new BasicDataSource();
            ds.setDriverClassName(statements.get("db.main.driver"));
            ds.setUrl(String.format("%s://%s:%s/%s",
                    statements.get("db.main.url"),
                    statements.get("db.main.host"),
                    statements.get("db.main.port"),
                    statements.get("db.main.name")));
            ds.setUsername(statements.get("db.main.username"));
            ds.setPassword(statements.get("db.main.password"));
            ds.setMinIdle(5);
            ds.setMaxIdle(10);
            ds.setMaxOpenPreparedStatements(100);
            ds.setMaxWait(10000L);
            dataSource = ds;
        }

        return dataSource;

    }

}
