package com.example.expositions1.services.impl;

import com.example.expositions1.controllers.converters.Converter;
import com.example.expositions1.controllers.converters.NewExpositionDataToModelConverter;
import com.example.expositions1.database.dao.JDBCDaoFactory;
import com.example.expositions1.database.dao.JDBCExpositionDao;
import com.example.expositions1.database.dao.JDBCHallDao;
import com.example.expositions1.database.dao.JDBCSessionDao;
import com.example.expositions1.dto.exposition.AddExpositionData;
import com.example.expositions1.exceptions.AvailableHallsNotFound;
import com.example.expositions1.exceptions.InternalDatabaseException;
import com.example.expositions1.models.ExpositionModel;
import com.example.expositions1.models.HallModel;
import com.example.expositions1.models.SessionModel;
import com.example.expositions1.services.SessionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SessionServiceImpl implements SessionService {

    private static final Logger LOG = LoggerFactory.getLogger(SessionServiceImpl.class);

    private JDBCDaoFactory daoFactory;
    private Converter<AddExpositionData, ExpositionModel> converter;


    public SessionServiceImpl() {
        this.daoFactory = JDBCDaoFactory.getInstance();
        converter = new NewExpositionDataToModelConverter();
    }

    @Override
    public void registerNewExpositionsInAvailableHalls(AddExpositionData expositionData) throws InternalDatabaseException, AvailableHallsNotFound {
        JDBCSessionDao sessionDao = daoFactory.createSessionDao();
        JDBCHallDao hallsDao = daoFactory.createHallDao();

        ExpositionModel exposition = converter.convert(expositionData);
        List<Integer> hallNumbers = expositionData.getHalls().stream()
                .map(Integer::parseInt).collect(Collectors.toList());
        List<HallModel> hallsToRegister = hallsDao.findByNumbers(hallNumbers);
        List<HallModel> availableHalls = hallsDao.findForExpositionDate(Date.valueOf(expositionData.getDate()));

        for (HallModel registerModel : hallsToRegister) {
            boolean matched = false;
            for (HallModel availableHall : availableHalls) {
                if (availableHall.getId().longValue() == registerModel.getId().longValue()) {
                    matched = true;
                }
            }
            if (!matched) {
                sessionDao.close();
                hallsDao.close();
                throw new AvailableHallsNotFound("Not found");
            }
        }

        List<SessionModel> sessions = new ArrayList<>();
        for (HallModel hall : hallsToRegister) {
            sessions.add(new SessionModel(exposition, hall));
        }

        try {
            sessionDao.saveAll(sessions);
        } catch (SQLException e) {
            e.printStackTrace();
            sessionDao.close();
            hallsDao.close();
            throw new InternalDatabaseException();
        }
        sessionDao.close();
        hallsDao.close();
    }

}
