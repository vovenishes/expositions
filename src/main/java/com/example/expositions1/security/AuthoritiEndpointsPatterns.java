package com.example.expositions1.security;

import java.util.List;

import static com.example.expositions1.constants.Endpoints.Regex.*;

public class AuthoritiEndpointsPatterns {

    public static final List<String> PERMITTED_ALL= List.of(EXPOSITION_LIST_R);
    public static final List<String> PERMITTED_GUEST= List.of(REGISTRATION_R, LOGIN_R);
    public static final List<String> PERMITTED_ADMIN=List.of(ADD_EXPOSITION_R, CANCEL_EXPOSITION_BY_ID_R);
    public static final List<String> PERMITTED_AUTHORIZED=List.of(LOGOUT_R);

}
