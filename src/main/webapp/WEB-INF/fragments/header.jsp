<%@ include file="/WEB-INF/fragments/taglibs.jsp" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="messages" scope="session"/>

<jsp:include page="./scripts.jsp" />

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class=" d-flex justify-content-xl-between sectionMainContent navbar-collapse" id="navbarSupportedContent">


        <ul class="navbar-nav mr-auto">

            <li class="nav-item active"><a class="navbar-brand" href='<c:url value="/"/>'>Expositions</a></li>

            <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" role="button"
                                             data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <fmt:message key="nav.link.lang" />
            </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="?lang=en"><fmt:message key="nav.link.lang.en" /></a>
                    <a class="dropdown-item" href="?lang=ru"><fmt:message key="nav.link.lang.ru" /></a>
                </div>
            </li>
        </ul>

        <div class="form-inline my-2 my-lg-0">
            <c:choose>
                <c:when test="${sessionScope.role == 'GUEST'}">
                    <div class="form-inline my-2 my-lg-0">
                        <a href='<c:url value="/login"/>' class="btn btn-link my-2 my-sm-0" id="signin">
                            <fmt:message key="nav.link.sign.in" />
                        </a> <a href='<c:url value="/registration"/>' class="mx-3 btn btn-success">
                        <fmt:message key="nav.link.sign.up" />
                    </a>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="form-inline my-2 my-lg-0">
                        <div class="navbar-nav mr-auto">
                            <div class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class=" mr-1"><c:out value="${sessionScope.username}"></c:out></span>
                                    <span>
										<c:if test="${sessionScope.role == 'ADMIN'}">
                                            <span  class="badge badge-info mr-1"><fmt:message key="role.admin" /></span>
                                        </c:if>
										<c:if test="${sessionScope.role == 'USER'}">
                                            <span  class="badge badge-success mr-1"><fmt:message key="role.user" /></span>
                                        </c:if>
									</span>
                                    <span  class="mr-1 bi bi-person-fill"> </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href='<c:url value="/logout"/>'><fmt:message key="nav.link.logout" /></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>




        </div>


    </div>
</nav>