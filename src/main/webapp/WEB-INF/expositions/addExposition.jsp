<%@ include file="/WEB-INF/fragments/taglibs.jsp" %>

<!DOCTYPE html>
<html>
<head>
    <jsp:include page="../fragments/head.jsp"/>
    <title><fmt:message key="title.expositions"/></title>

</head>
<body>

<jsp:include page="../fragments/header.jsp"/>

<div class="text-center">
    <div class="sectionMainContent">


        <form class="form-signin" action='<c:url value="/add"/>' method="post">

            <h1 class="h3 mb-3 mt-5 font-weight-normal"><fmt:message key="exposition.add.new"/></h1>

            <c:if test="${errorAddExposition != null}">
                <div class="alert alert-danger">
                    <c:out value="${errorAddExposition}"></c:out>
                </div>
            </c:if>

            <c:if test="${errorAddExpositionNotValid != null}">
                <div class="alert alert-danger">
                    <c:out value="${errorAddExpositionNotValid}"></c:out>
                </div>
            </c:if>

            <div class=" pb-4 justify-content-center">
                <input type="text" class="form-control" placeholder="<fmt:message key="exposition.title" />"
                       name="title" value="<c:out value="${exposition.title}" />">
            </div>

            <div class=" pb-4 justify-content-center">
                <input type="text" class="form-control" placeholder="<fmt:message key="exposition.cost" />"
                       name="cost" value="<c:out value="${exposition.cost}" />">
            </div>

            <div class=" pb-4 justify-content-center">
                <input type="date" class="form-control" placeholder="<fmt:message key="exposition.date" />"
                       name="date" value="<c:out value="${exposition.date}" />">
            </div>

            <c:choose>
                <c:when test="${fn:length(exposition.halls) == 0}">
                    <button class="btn btn-primary mt-4" type="submit"><fmt:message key="get.available.halls.for.timeslots"/></button>

                    <c:if test="${warnHallsNotFound != null && exposition.date != null && exposition.cost != null && exposition.title != null}">
                        <div class="alert alert-warning mt-4">
                            <c:out value="${warnHallsNotFound}"></c:out>
                        </div>
                    </c:if>
                </c:when>
                <c:otherwise>
                        <c:forEach var="hall" items="${exposition.halls}">
                            <!-- primary -->
                            <div class="pretty p-default">
                                <input type="checkbox" name="halls" value="${hall}" id="inlineCheckbox${hall}"/>
                                <div class="state p-primary">
                                    <label>
                                        <fmt:message key="hall.number">&nbsp;</fmt:message>
                                        <c:out value="${hall}"/>
                                    </label>
                                </div>
                            </div>
                            <br>
                        </c:forEach>
                    <button class="btn btn-primary mt-4" type="submit"><fmt:message key="add.exposition"/></button>
                </c:otherwise>
            </c:choose>
        </form>
    </div>
</div>

</body>
</html>