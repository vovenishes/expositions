<!-- <%@ include file="/WEB-INF/fragments/taglibs.jsp" %> -->

<!DOCTYPE html>
<html>
<head>
    <jsp:include page="../fragments/head.jsp"/>
    <title><fmt:message key="title.reg"/></title>
    <link rel="stylesheet" href="/mainregistration.css">
    <link rel="stylesheet" href="/normalizeregistration.css">
</head>
<body>
<jsp:include page="../fragments/header.jsp"/>

<%--<!-- <style><%@ include file="../resources/mainregistration.css" %></style>--%>
<%--<style><%@ include file="../resources/normalizeregistration.css" %></style> -->--%>


<div class="topic">
    <div class="header">
        <div class="container">

        </div>
    </div>
    <section class="form">
        <div class="container">
            <div class="form__inner">
                <h2 class="form__title">Registration</h2>

                <c:if test="${errorUserRegister != null}">
                    <div class="alert alert-danger">
                        <c:out value="${errorUserRegister}"></c:out>
                    </div>
                </c:if>

                <form class="form__user" method="post" action='<c:url value="/registration"/>'>
                    <input class="form__input" type="text" placeholder='<fmt:message key="user.username"/>'
                           name="username" value='<c:out value="${user.username }"/>'>
                    <input class="form__input" type="password" placeholder='<fmt:message key="user.password"/>'
                           name="password" value='<c:out value="${user.password }"/>'>
                    <label for="user_radio"><fmt:message key="role.user"/></label>
                    <input id="user_radio" class="form__input-radio" type="radio" value="USER" name=role>
                    <label for="admin_radio"><fmt:message key="role.admin"/></label>
                    <input id="admin_radio" class="form__input-radio" type="radio" value="ADMIN" name=role>
                    <button class="form__btn" type="submit">Registration</button>
                </form>
            </div>
        </div>
    </section>
    <footer class="footer"></footer>
</div>
</html>

</body>
</html>







<%--<%@ include file="/WEB-INF/fragments/taglibs.jsp" %>--%>

<%--<!DOCTYPE html>--%>
<%--<html>--%>
<%--<head>--%>
<%--    <jsp:include page="../fragments/head.jsp"/>--%>
<%--    <title><fmt:message key="title.reg"/></title>--%>

<%--</head>--%>
<%--<body>--%>
<%--<jsp:include page="../fragments/header.jsp"/>--%>

<%--<div class="topic">--%>
<%--    <div class="header">--%>
<%--        <div class="container">--%>

<%--        </div>--%>
<%--    </div>--%>
<%--    <section class="form">--%>
<%--        <div class="container">--%>
<%--            <div class="form__inner">--%>
<%--                <h2 class="form__title">Registration</h2>--%>

<%--                <form class="form__user" method="post" action='<c:url value="/registration"/>'>--%>
<%--                    <input class="form__input" type="text" placeholder="<fmt:message key="user.username"/>"--%>
<%--                           name="username" value="<c:out value="${user.username }" />">--%>
<%--                    <input class="form__input" type="password" placeholder="<fmt:message key="user.password"/>"--%>
<%--                           name="password" value="<c:out value="${user.password }" />">--%>


<%--&lt;%&ndash;                    <input id="guest_radio" class="form__input" type="radio" value="GUEST" name=role>&ndash;%&gt;--%>
<%--&lt;%&ndash;                    <label for="guest_radio"><fmt:message key="role.guest"/></label>&ndash;%&gt;--%>

<%--                    <input id="user_radio" class="form__input" type="radio" value="USER" name=role>--%>
<%--                    <label for="user_radio"><fmt:message key="role.user"/></label>--%>

<%--                    <input id="admin_radio" class="form__input" type="radio" value="ADMIN" name=role>--%>
<%--                    <label for="admin_radio"><fmt:message key="role.admin"/></label>--%>
<%--                    <button class="form__btn" type="submit">Registration</button>--%>
<%--                </form>--%>
<%--            </div>--%>
<%--        </div>--%>

<%--    </section>--%>
<%--    <footer class="footer"></footer>--%>
<%--</div>--%>
<%--</html>--%>

<%--</body>--%>
<%--</html>--%>
